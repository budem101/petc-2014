#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 18:23:58 2020

The mathematical model of the photosynthetic electron transport chain defines methods to calculate reaction rates
and set of ten differential equations based on the model published by Ebenhoeh et al. in 2014

Copyright (C) 2014-2018  Anna Matuszyńska, Oliver Ebenhöh

This program is free software: you can redistribute and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details. You should have received a copy of the GNU General Public License
along with this program (license.txt).  If not, see <http://www.gnu.org/licenses/>.
"""
__author__ = "Anna Matuszyńska"
__copyright__ = "Copyright 2018, Heinrich-Heine University Dusseldorf"
__credits__ = ["Anna Matuszynska", "Oliver Ebenhoeh"]
__maintainer__ = "Anna Matuszynska"
__email__ = "Anna.Matuszynska@uni-duesseldorf.de"
__status__ = "Production/Stable"

# Import built-in libraries
from modelbase.ode import Model, Simulator
import matplotlib.pyplot as plt
import model

# Set initial conditions using dictionary
y0 = {#"B":  m.get_parameter("PSIItot"),  #photosystem II protein concentration
      "PQ":17.5,  # oxidised plastoquinone
      "PC":0.0202,  # oxidised plastocyan
      "Fd":5.0,  # oxidised ferrodoxin
      "ATP": 0.0,# stromal concentration of ATP
      "NADPH": 0.0,  # stromal concentration of NADPH
      "H": model.calculate_pHinv(7.2),  # lumenal protons
      "LHC": 0.9}

s = Simulator(model.m)

s.initialise(y0)
s.update_parameters({'Ton': 270, 'Toff':900, 'dT': 90})
                    
t=0
Tmax=1800
while t < Tmax :   
    #turn on the saturating pulse of light of Tflash length
    if t%s.model.get_parameter('dT') == 0:
        s.update_parameter('pfd', 5000)
        s.simulate(t+0.8, steps=1000)
    else:
        #switch on the light except for the dark period
        #t+dT-Tflash is the time to the next flash
        if t< s.model.get_parameter('Ton') or t>s.model.get_parameter('Toff'):
            s.update_parameter('pfd', 0.0001)
        else:
            #put the actinic light
            s.update_parameter('pfd', 100)
        new_t = t+s.model.get_parameter('dT')-0.8
        s.simulate(new_t, **{"atol":1.0e-10})  #I needed to make it smaller from the default, otherwise integration problems  
    t = s.get_time()[-1]           


# Needs to be normalized s.plot_selection('Fluo')
f = max(s.get_variable('Fluo'))
plt.figure(1)
plt.plot(s.get_time(), s.get_variable('Fluo')/f)
plt.xlabel('time [s]')
plt.ylim(0,1)
plt.ylabel('Fluorescence normalized to Fm')    

plt.figure(2)
plt.plot(s.get_time(), s.get_variable('PQred')/s.model.get_parameter('PQtot'), 'r', label='reduced PQ')
plt.plot(s.get_time(), s.get_variable('ps2cs'), 'b', label='relative cross-section of PSII')
plt.xlabel('time [s]')
plt.yticks([0, 0.25, .5, .6, .7, .8, .9, 1],[0, 25, 50, 60, 70, 80, 90, 100])
plt.ylabel('% of the total pool')
plt.legend()
plt.show()



